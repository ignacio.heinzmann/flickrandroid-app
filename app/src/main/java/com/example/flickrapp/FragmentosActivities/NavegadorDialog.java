package com.example.flickrapp.FragmentosActivities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import android.widget.Toast;


public class NavegadorDialog extends DialogFragment {

    private String urlFoto;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        String[] arrayMedios = {"Abrir en navegador Web"};

        if (getArguments() != null) {
            if (getArguments().getBoolean("notAlertDialog")) {
                return super.onCreateDialog(savedInstanceState);
            }
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Abrir");
        builder.setItems(arrayMedios, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which) {
                try {
                    Uri uri = Uri.parse(urlFoto);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
                catch (Exception e){
                    Toast.makeText(getActivity(),"No  te encuentras con una conexion a internet", Toast.LENGTH_SHORT);
                }

            }
        });

        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }
}