package com.example.flickrapp.FragmentosActivities;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.flickrapp.ClasesModelo.Album;
import com.example.flickrapp.ClasesModelo.Comentario;
import com.example.flickrapp.ClasesModelo.Foto;
import com.example.flickrapp.Datos.DataManagerAplication;
import com.example.flickrapp.Datos.DatosFlicker;
import com.example.flickrapp.Datos.MemoriaInterna.ImagenMemoria;
import com.example.flickrapp.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class FotoIndv extends Fragment implements View.OnClickListener, DatosFlicker.DatosFlickerListener {
    private FloatingActionButton sharebtn;
    private OnButtonClickedListener mCallBack;


    private static final String TAG = "FotoIndvFragment";
    protected RecyclerView.LayoutManager mLayoutManager;
    private ImageView imgFoto;
    private TextView txtTitulo;
    private TextView txtAutor;
    private TextView txtAlbum;

    protected CustomAdapterComentarios mAdapter;
    protected ArrayList<Comentario> mDataSet = new ArrayList<Comentario>();
    private Foto foto;
    private RecyclerView rcComentarios;

    public FotoIndv(Foto foto){
        this.foto=foto;
    }

    public FotoIndv() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.photo_view, container, false);
        sharebtn = (FloatingActionButton) rootView.findViewById(R.id.sharebtn);
        sharebtn.setOnClickListener(this);
        imgFoto = (ImageView) rootView.findViewById(R.id.photoView);
        txtTitulo = (TextView) rootView.findViewById(R.id.textViewNombreFoto);
        txtAlbum = (TextView) rootView.findViewById(R.id.textViewAutor);
        txtAutor = (TextView) rootView.findViewById(R.id.textViewAlbum);
        rcComentarios = (RecyclerView) rootView.findViewById(R.id.recyclerViewComentarios);

        mLayoutManager = new GridLayoutManager(getActivity(),1);
        rcComentarios.setHasFixedSize(true);
        rcComentarios.setLayoutManager(mLayoutManager);
        mAdapter = new CustomAdapterComentarios(mDataSet);
        rcComentarios.setAdapter(mAdapter);
        ((DataManagerAplication)getActivity().getApplication()).getAlbumesRepository().getAlbumesById(foto.getIdAlbum()).observe(this, new Observer<List<Album>>(){
            @Override
            public void onChanged(List<Album> albumes) {

                txtAlbum.setText("Nombre del album: " + albumes.get(0).getTitle());
                txtAutor.setText("Autor: " + albumes.get(0).getUsername());

            }
        });

        imgFoto.setImageURI(Uri.parse(new ImagenMemoria().cargarImagen(foto.getIdFoto()).toString()));
        txtTitulo.setText(foto.getTitle());
        imgFoto.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                NavegadorDialog mostrarNav = new NavegadorDialog();
                mostrarNav.setUrlFoto("https://farm6.staticflickr.com/5718/"+ foto.getIdFoto() +"_"+ foto.getSecret() +".jpg");
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("navDialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);
                mostrarNav.show(ft, "navDialog");
                return false;
            }
        });

        iniciarDataSet();

        return rootView;
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallBack = (OnButtonClickedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " debe implementar OnButtonClickedListener ");
        }
    }

    public void iniciarDataSet(){
        DatosFlicker.loadComentarios(foto,this);
    }

    @Override
    public void onAlbumesLoaded(ArrayList<Album> albumes) {
    }

    @Override
    public void onFotosLoaded(ArrayList<Foto> fotos) {
    }

    @Override
    public void onComentariosLoaded(ArrayList<Comentario> comentarios) {
        this.mDataSet = comentarios;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter.setmDataSet(mDataSet);
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    public interface OnButtonClickedListener{
        public void onButtonShareClick(View view, Foto foto);
    }

    @Override
    public void onClick(View v) {
        mCallBack.onButtonShareClick(v, this.foto);
    }

}
