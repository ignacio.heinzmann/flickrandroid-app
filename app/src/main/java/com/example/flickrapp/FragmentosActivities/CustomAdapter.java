package com.example.flickrapp.FragmentosActivities;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.flickrapp.ClasesModelo.Foto;
import com.example.flickrapp.Datos.MemoriaInterna.ImagenMemoria;
import com.example.flickrapp.R;

import java.util.ArrayList;


public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ImageViewHolder> {
    private static final String TAG = "CustomAdapter";

    protected ArrayList<Foto> mDataSet = new ArrayList<Foto>();

    public void setmDataSet(ArrayList<Foto> mDataSet) {
        this.mDataSet = mDataSet;

    }

    public static class ImageViewHolder extends RecyclerView.ViewHolder {
        private final ImageView Photo;
        private final TextView PhotoName;

        public ImageViewHolder(View v) {
            super(v);
            Photo = v.findViewById(R.id.imageViewPhoto);
            PhotoName = v.findViewById(R.id.textViewFileName);
        }

        public ImageView getPhoto() {
            return Photo;
        }

        public TextView getPhotoName() {
            return PhotoName;
        }
    }

    public CustomAdapter(ArrayList<Foto> mDataSet) {
        this.mDataSet = mDataSet;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.photorecycle, viewGroup, false);
        ImageViewHolder imageViewHolder = new ImageViewHolder(v);

        return imageViewHolder;
    }

    @Override
    public void onBindViewHolder(ImageViewHolder viewHolder, final int position) {
        if(mDataSet.get(position) != null){
            viewHolder.getPhoto().setImageURI(Uri.parse(new ImagenMemoria().cargarImagen(mDataSet.get(position).getIdFoto()).toString()));
            viewHolder.getPhotoName().setText(mDataSet.get(position).getTitle());
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                FragmentActivity frag = (FragmentActivity) view.getContext();
                FotoIndv fragindv = new FotoIndv(mDataSet.get(position));
                frag.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragindv).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }
}
