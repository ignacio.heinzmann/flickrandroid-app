package com.example.flickrapp.FragmentosActivities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.flickrapp.ClasesModelo.Album;
import com.example.flickrapp.ClasesModelo.Comentario;
import com.example.flickrapp.ClasesModelo.Foto;
import com.example.flickrapp.Datos.DataManagerAplication;
import com.example.flickrapp.Datos.DatosFlicker;
import com.example.flickrapp.R;

import java.util.ArrayList;
import java.util.List;


public class FragFotos extends Fragment implements View.OnClickListener {

    private static final String TAG = "RecyclerViewFragment";

    protected RecyclerView mRecyclerView;
    protected CustomAdapter mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;
    private OnButtonClickedListener mCallBack;
    private ImageButton imageButtonBack;
    private CheckBox chbxNombreFoto;
    private TextView titulo;
    protected ArrayList<Foto> mDataSet = new ArrayList<Foto>();
    protected ArrayList<Foto> mDataSetOrdenado = new ArrayList<Foto>();
    protected Album albumSelec;

    public FragFotos(Album albumSelec) {
        this.albumSelec = albumSelec;
    }
    public FragFotos() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fotos, container, false);
        rootView.setTag(TAG);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerViewPhoto);
        imageButtonBack = (ImageButton) rootView.findViewById(R.id.imageButtonBack);
        imageButtonBack.setOnClickListener(this);
        chbxNombreFoto = (CheckBox) rootView.findViewById(R.id.checkBoxNombre);
        chbxNombreFoto.setOnClickListener(this);
        titulo = (TextView) rootView.findViewById(R.id.tituloFragFotos);
        titulo.setText(albumSelec.getTitle());

        setRecyclerViewLayoutManager();
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new CustomAdapter(mDataSet);
        mRecyclerView.setAdapter(mAdapter);

        iniciarDataSet();

        return rootView;
    }
    public void iniciarDataSet(){
        ((DataManagerAplication)getActivity().getApplication()).getFotosRepository().getFotosByAlbumId(albumSelec.getId()).observe(this, new Observer<List<Foto>>(){
            @Override
            public void onChanged(List<Foto> fotos) {
                mDataSet = (ArrayList<Foto>) fotos;
                mAdapter.setmDataSet((ArrayList<Foto>) fotos);
                mAdapter.notifyDataSetChanged();
            }
        });

    }

    public void setRecyclerViewLayoutManager() {
        int scrollPosition = 0;

        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        mLayoutManager = new GridLayoutManager(getActivity(),2);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }


    public interface OnButtonClickedListener{
        public void onButtonAtrasClick();
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallBack = (OnButtonClickedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " debe implementar OnButtonClickedListener ");
        }

    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.imageButtonBack){
            mCallBack.onButtonAtrasClick();
        }
        if (v.getId() == R.id.checkBoxNombre){
            if(((CheckBox) v).isChecked()){
                ((DataManagerAplication)getActivity().getApplication()).getFotosRepository().getFotosByNombre(albumSelec.getId()).observe(this, new Observer<List<Foto>>(){
                    @Override
                    public void onChanged(List<Foto> fotos) {
                        mDataSetOrdenado = (ArrayList<Foto>) fotos;
                        mAdapter.setmDataSet((ArrayList<Foto>) fotos);
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }
            else{
                mAdapter.setmDataSet(mDataSet);
                mAdapter.notifyDataSetChanged();
            }
        }
    }
}