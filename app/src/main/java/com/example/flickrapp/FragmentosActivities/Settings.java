package com.example.flickrapp.FragmentosActivities;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import com.example.flickrapp.R;

public class Settings extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }

}
