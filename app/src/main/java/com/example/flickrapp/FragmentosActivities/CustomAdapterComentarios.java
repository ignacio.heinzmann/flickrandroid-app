package com.example.flickrapp.FragmentosActivities;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.flickrapp.ClasesModelo.Comentario;
import com.example.flickrapp.R;

import java.util.ArrayList;

public class CustomAdapterComentarios extends RecyclerView.Adapter<CustomAdapterComentarios.ViewHolder>{
    protected ArrayList<Comentario> mDataSet = new ArrayList<Comentario>();

    public CustomAdapterComentarios(ArrayList<Comentario> comentarioArrayList){
        this.mDataSet = comentarioArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comentarioview, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.getTxtAutorView().setText(mDataSet.get(position).getNombreAutor());
        holder.getTxtContenidoView().setText(mDataSet.get(position).getContenidoComentario());
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView txtAutor;
        private final TextView txtContenido;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtAutor = itemView.findViewById(R.id.txtAutor);
            txtContenido = itemView.findViewById(R.id.txtContenido);
        }
        public TextView getTxtAutorView() {
            return txtAutor;
        }
        public TextView getTxtContenidoView() {
            return txtContenido;
        }
    }

    public void setmDataSet(ArrayList<Comentario> mDataSet) {
        this.mDataSet = mDataSet;
    }
}
