package com.example.flickrapp.FragmentosActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.flickrapp.ClasesModelo.Album;
import com.example.flickrapp.ClasesModelo.Comentario;
import com.example.flickrapp.ClasesModelo.Foto;
import com.example.flickrapp.Datos.AlbumesRepository;
import com.example.flickrapp.Datos.DataManagerAplication;
import com.example.flickrapp.Datos.DatosFlicker;
import com.example.flickrapp.Datos.FotosRepository;
import com.example.flickrapp.Datos.MemoriaInterna.ImagenMemoria;
import com.example.flickrapp.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, DatosFlicker.DatosFlickerListener, ImagenMemoria.ImagenMemoriaListener {

    private Button btnEntrar;
    private ArrayList<Album> mDataSet = new ArrayList<>();
    private int albumesCargados = 0;
    private CargaDialog dialogoCarga;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnEntrar = (Button) findViewById(R.id.btnEntrar);
        btnEntrar.setOnClickListener(this);

        Toast toast;
        if(((DataManagerAplication)getApplication()).isNetworkConnected())
        {
            dialogoCarga = new CargaDialog();

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment prev = getSupportFragmentManager().findFragmentByTag("CargaDialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);
            dialogoCarga.show(ft, "CargaDialog");
            iniciarDataSet();
        }else{
            toast = Toast.makeText(getApplication(),"Cargando datos desde memoria interna",Toast.LENGTH_SHORT);
            btnEntrar.setEnabled(true);
            toast.show();
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;
        if(view.getId()==R.id.btnEntrar){
            intent = new Intent(this,MainActivity2.class);
            startActivity(intent);
        }
    }

    public void iniciarDataSet(){
        DatosFlicker.loadAlbumes(this, getString(R.string.urlAlbumes));
    }

    @Override
    public void onAlbumesLoaded(ArrayList<Album> albumes) {
        DatosFlicker.DatosFlickerListener fragmento = this;
        this.mDataSet = albumes;
        Thread hilo = new Thread(new Runnable() {
            @Override
            public void run() {
                for(Album album:albumes){
                    DatosFlicker.loadFotos(album,fragmento);
                }
            }
        });
        hilo.start();
    }

    @Override
    public void onFotosLoaded(ArrayList<Foto> fotos) {
        AlbumesRepository albumesRepository = ((DataManagerAplication)getApplication()).getAlbumesRepository();
        FotosRepository fotosRepository = ((DataManagerAplication)getApplication()).getFotosRepository();
        for (Album album : mDataSet){
            if(album.getIdPrimeraFoto() != null){
                albumesRepository.insert(album);
            }
        }
        albumesCargados++;
        ImagenMemoria imgMemoria = new ImagenMemoria();
        for(int j = 0; j < fotos.size(); j++ ){
            fotosRepository.insert(fotos.get(j));
            imgMemoria.guardarImagen(this,fotos.get(j),this);
        }
    }

    @Override
    public void onComentariosLoaded(ArrayList<Comentario> comentarios) {
    }

    @Override
    public void onFotosCargadas() {
        if(albumesCargados == mDataSet.size()){
            albumesCargados = 0;
            dialogoCarga.dismiss();
            runOnUiThread(new Thread(new Runnable() {
                @Override
                public void run() {
                    btnEntrar.setEnabled(true);
                }
            }));
        }
    }
}