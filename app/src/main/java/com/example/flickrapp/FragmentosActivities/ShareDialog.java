package com.example.flickrapp.FragmentosActivities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.flickrapp.ClasesModelo.Foto;

public class ShareDialog extends DialogFragment {

    private Foto fotoSelected;
    private String urlFotoSelected;

    public ShareDialog(Foto foto){
        this.fotoSelected = foto;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        urlFotoSelected = "https://farm6.staticflickr.com/5718/"+ fotoSelected.getIdFoto() +"_"+ fotoSelected.getSecret() +".jpg";
        String[] arrayMedios = {"Whatsapp","Gmail","Snapchat"};
        if (getArguments() != null) {
            if (getArguments().getBoolean("notAlertDialog")) {
                return super.onCreateDialog(savedInstanceState);
            }
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Compartir");
        builder.setItems(arrayMedios, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which) {
                Toast toast;
                switch (which){
                    case 0:
                        toast = Toast.makeText(getContext(),"Esta opción no está implementada",Toast.LENGTH_SHORT);
                        toast.show();
                        break;
                    case 1:
                        String[] recipients=new String[]{"ignacio.heinzmann@gmail.com"};

                        String subject="Image Link";

                        String content ="Here you have the image link: "+urlFotoSelected;

                        Intent intentEmail = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:"));
                        intentEmail.putExtra(Intent.EXTRA_EMAIL, recipients);
                        intentEmail.putExtra(Intent.EXTRA_SUBJECT, subject);
                        intentEmail.putExtra(Intent.EXTRA_TEXT, content);

                        intentEmail.setType("text/plain");

                        startActivity(Intent.createChooser(intentEmail, "Choose an email client from..."));
                        break;
                    case 2:
                        toast = Toast.makeText(getContext(),"Esta opción no está implementada",Toast.LENGTH_SHORT);
                        toast.show();
                        break;
                    case 3:
                        toast = Toast.makeText(getContext(),"Esta opción no está implementada",Toast.LENGTH_SHORT);
                        toast.show();
                        break;
                }
            }
        });

        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

}
