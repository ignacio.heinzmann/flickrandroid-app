package com.example.flickrapp.FragmentosActivities;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.example.flickrapp.ClasesModelo.Album;
import com.example.flickrapp.ClasesModelo.Comentario;
import com.example.flickrapp.ClasesModelo.Foto;
import com.example.flickrapp.Datos.DatosFlicker;
import com.example.flickrapp.R;

import java.util.ArrayList;


public class Albumes extends Fragment implements View.OnClickListener {

    private static final String TAG = "AlbumFragment";
    private OnButtonClickedListenerAlbum mCallBack;
    private Button btnBuscar;
    private EditText txtBuscar;
    private ImageButton btnAtras;
    private ImageButton btnConfig;
    protected RecyclerView mRecyclerView;
    protected CustomAdapterAlbumes mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;
    protected ArrayList<Album> mDataSet = new ArrayList<Album>();


    public Albumes() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_albumes, container, false);
        v.setTag(TAG);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycleAlbumes);
        btnBuscar = (Button) v.findViewById(R.id.btnBuscarAlbum);
        btnAtras = (ImageButton) v.findViewById(R.id.btnAtras);
        txtBuscar = (EditText) v.findViewById(R.id.txtBuscarAlbum);
        btnConfig = (ImageButton) v.findViewById(R.id.btnConfig);

        setRecyclerViewLayoutManager();
        mAdapter = new CustomAdapterAlbumes(mDataSet);
        mRecyclerView.setAdapter(mAdapter);

        btnAtras.setOnClickListener(this);
        btnConfig.setOnClickListener(this);

        return v;
    }

    public void setRecyclerViewLayoutManager() {
        int scrollPosition = 0;
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((GridLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }
        mLayoutManager = new GridLayoutManager(getActivity(),2);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }

    public interface OnButtonClickedListenerAlbum{
        public void onButtonSkipClickAlbum();
        public void onButtonSettingsClick();
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallBack = (OnButtonClickedListenerAlbum) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " debe implementar OnButtonClickedListener ");
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.btnAtras){
        mCallBack.onButtonSkipClickAlbum();
        }
        if (v.getId() == R.id.btnConfig) {
            mCallBack.onButtonSettingsClick();
        }
    }

    public void setmDataSet(ArrayList<Album> albumList){
        this.mDataSet = albumList;
        mAdapter.setmDataSet(mDataSet);
        mAdapter.notifyDataSetChanged();
    }
}