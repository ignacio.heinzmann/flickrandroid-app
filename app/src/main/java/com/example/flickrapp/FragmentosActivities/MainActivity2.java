package com.example.flickrapp.FragmentosActivities;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.flickrapp.ClasesModelo.Album;
import com.example.flickrapp.ClasesModelo.Foto;
import com.example.flickrapp.Datos.DataManagerAplication;
import com.example.flickrapp.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity2 extends FragmentActivity implements  Albumes.OnButtonClickedListenerAlbum, FragFotos.OnButtonClickedListener, FotoIndv.OnButtonClickedListener{
    private ShareDialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        if (findViewById(R.id.fragment_container) != null) {
            if (savedInstanceState != null) {
                return;
            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            Albumes albumFrag = new Albumes();
            transaction.replace(R.id.fragment_container,albumFrag);
            transaction.commit();
            ((DataManagerAplication)getApplication()).getAlbumesRepository().getAlbumes().observe(this, new Observer<List<Album>>(){
                @Override
                public void onChanged(List<Album> album) {
                    albumFrag.setmDataSet((ArrayList<Album>) album);
                }
            } );
        }
    }

    @Override
    public void onButtonSkipClickAlbum() {
        finish();
    }

    @Override
    public void onButtonAtrasClick() {
        getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onButtonShareClick(View view, Foto foto){
        if(view.getId()==R.id.sharebtn)
        {
            myDialog = new ShareDialog(foto);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);
            myDialog.show(ft, "dialog");
        }
    }

    @Override
    public void onButtonSettingsClick(){
        Intent modifySettings=new Intent(MainActivity2.this,Settings.class);
        startActivity(modifySettings);
    }
}