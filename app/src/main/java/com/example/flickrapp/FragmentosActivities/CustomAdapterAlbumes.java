package com.example.flickrapp.FragmentosActivities;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.flickrapp.ClasesModelo.Album;
import com.example.flickrapp.Datos.MemoriaInterna.ImagenMemoria;
import com.example.flickrapp.R;

import java.util.ArrayList;


public class CustomAdapterAlbumes extends RecyclerView.Adapter<CustomAdapterAlbumes.ViewHolder>{
    private static final String TAG = "CustomAdapter";
    private ArrayList<Album> mDataSet;
    private FragFotos fragfotos;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.albumrecycle, viewGroup, false);
        return new ViewHolder(v);
    }

    public CustomAdapterAlbumes(ArrayList<Album> dataSet) {
        mDataSet = dataSet;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if(mDataSet.get(position).getIdPrimeraFoto() != null){
            holder.getImagen().setImageURI(Uri.parse(new ImagenMemoria().cargarImagen(mDataSet.get(position).getIdPrimeraFoto()).toString()));
        }

        String titulo = mDataSet.get(position).getTitle();
        holder.getTituloAlbum().setText(titulo);

        holder.itemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                FragmentActivity frag = (FragmentActivity) view.getContext();
                fragfotos = new FragFotos(mDataSet.get(position));
                FragmentTransaction ft = frag.getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, fragfotos).commit();
                ft.addToBackStack(null);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private final ImageView imgFotoAlbum;
        private final TextView txtTituloAlbum;

        public ViewHolder(View v) {
            super(v);
            imgFotoAlbum = (ImageView) v.findViewById(R.id.imgFotoAlbum);
            txtTituloAlbum = (TextView) v.findViewById(R.id.txtTituloAlbum);
        }
        public TextView getTituloAlbum() {
            return txtTituloAlbum;
        }
        public ImageView getImagen() {
            return imgFotoAlbum;
        }
    }

    public void setmDataSet(ArrayList<Album> mDataSet) {
        this.mDataSet = mDataSet;
    }
}
