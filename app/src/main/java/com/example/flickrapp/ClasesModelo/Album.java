package com.example.flickrapp.ClasesModelo;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity(tableName = "Albumes")
public class Album {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "idAlbum")
    private String id;

    @ColumnInfo(name = "owner")
    private String owner;

    @ColumnInfo(name = "username")
    private String username;

    @ColumnInfo(name = "secret")
    private String secret;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "idPrimeraFoto")
    private String idPrimeraFoto;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIdPrimeraFoto() {
        return idPrimeraFoto;
    }

    public void setIdPrimeraFoto(String idPrimeraFoto) {
        this.idPrimeraFoto = idPrimeraFoto;
    }

}
