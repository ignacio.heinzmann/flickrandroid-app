package com.example.flickrapp.ClasesModelo;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = "Fotos",primaryKeys = {"idFoto", "idAlbum"})
public class Foto {

    @NonNull
    @ColumnInfo(name = "idFoto")
    private String idFoto;

    @ColumnInfo(name = "secret")
    private String secret;

    @ColumnInfo(name = "title")
    private String title;

    @NonNull
    @ColumnInfo(name = "idAlbum")
    private String idAlbum;

    public String getIdAlbum() {
        return idAlbum;
    }

    public void setIdAlbum(String idAlbum) {
        this.idAlbum = idAlbum;
    }

    public String getIdFoto() {
        return idFoto;
    }

    public void setIdFoto(String idFoto) {
        this.idFoto = idFoto;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
