package com.example.flickrapp.Datos.MemoriaInterna;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.example.flickrapp.ClasesModelo.Foto;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class ImagenMemoria {
    ContextWrapper cw;
    File dirImages;

    public String guardarImagen(Context context, Foto foto, ImagenMemoriaListener iml){
        cw = new ContextWrapper(context);
        this.dirImages = cw.getDir("Pictures", Context.MODE_PRIVATE);
        File archivoFoto = new File( dirImages, foto.getIdFoto() + ".jpg");
        try {
            FileOutputStream archivoGuardar = new FileOutputStream(archivoFoto);
            String url = "https://farm6.staticflickr.com/5718/"+ foto.getIdFoto() +"_"+ foto.getSecret() +".jpg";
            URL urlImagen = new URL(url);
            Thread hilo = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        HttpURLConnection conn = (HttpURLConnection) urlImagen.openConnection();
                        conn.connect();
                        Bitmap imagen = BitmapFactory.decodeStream(conn.getInputStream());
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        imagen.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] byteArray = stream.toByteArray();
                        archivoGuardar.write(byteArray);
                        archivoGuardar.close();
                        iml.onFotosCargadas();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            hilo.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return archivoFoto.getAbsolutePath();
    }

    public File cargarImagen(String idFoto){
        return new File("/data/user/0/com.example.flickrapp/app_Pictures/", idFoto + ".jpg");
    }

    public interface ImagenMemoriaListener{
        public void onFotosCargadas();
    }
}
