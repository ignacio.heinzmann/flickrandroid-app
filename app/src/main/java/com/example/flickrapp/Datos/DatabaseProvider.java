package com.example.flickrapp.Datos;

import android.content.Context;

import androidx.room.Room;


public class DatabaseProvider {
    private static FlickrRoomDatabase instance;

    public static FlickrRoomDatabase getDatabase(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    FlickrRoomDatabase.class, "flickr_database").fallbackToDestructiveMigration().build();
        }
        return instance;
    }
}
