package com.example.flickrapp.Datos;

import android.content.Context;

import androidx.lifecycle.LiveData;

import com.example.flickrapp.ClasesModelo.Album;

import java.util.List;

public class AlbumesRepository {
    private IAlbumDao mIAlbumDao;
    private LiveData<List<Album>> mAlbum;

    public LiveData<List<Album>> getAlbumes() {
        mAlbum = mIAlbumDao.getAllAlbumes();

        return mAlbum;
    }

    public LiveData<List<Album>> getAlbumesById(String idAlbum) {
        mAlbum = mIAlbumDao.getAlbumesById(idAlbum);
        return mAlbum;
    }

    public AlbumesRepository(Context context) {
        FlickrRoomDatabase db = DatabaseProvider.getDatabase(context);
        mIAlbumDao = db.albumDao();
    }

    public void insert(final Album album) {
        new Thread() {
            @Override
            public void run() {
                mIAlbumDao.insert(album);
            }
        }.start();
    }

    public void update(final Album album) {
        new Thread() {
            @Override
            public void run() {
                mIAlbumDao.update(album);
            }
        }.start();
    }

    public void delete(final Album album) {
        new Thread() {
            @Override
            public void run() {
                mIAlbumDao.delete(album);
            }
        }.start();
    }
}
