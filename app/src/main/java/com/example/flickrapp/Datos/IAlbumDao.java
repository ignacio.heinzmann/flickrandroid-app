package com.example.flickrapp.Datos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.flickrapp.ClasesModelo.Album;
import com.example.flickrapp.ClasesModelo.Foto;

import java.util.List;

@Dao
public interface IAlbumDao {
    @Query("SELECT * from Albumes ORDER BY title ASC")
    LiveData<List<Album>> getAlbumByTitle();

    @Query("SELECT * from Albumes")
    LiveData<List<Album>> getAllAlbumes();

    @Query("SELECT * from Albumes WHERE idAlbum = :idAlbum")
    LiveData<List<Album>> getAlbumesById(String idAlbum);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Album album);

    @Update
    void update(Album album);

    @Delete
    void delete(Album album);

    @Query("DELETE FROM Albumes")
    void deleteAll();
}
