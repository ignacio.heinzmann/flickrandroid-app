package com.example.flickrapp.Datos;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.flickrapp.ClasesModelo.Album;
import com.example.flickrapp.ClasesModelo.Comentario;
import com.example.flickrapp.ClasesModelo.Foto;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;

public class DatosFlicker {

    public interface DatosFlickerListener{
        public void onAlbumesLoaded(ArrayList<Album> albumes);
        public void onFotosLoaded(ArrayList<Foto> fotos);
        public void onComentariosLoaded(ArrayList<Comentario> comentarios);
    }

    public static void loadAlbumes(DatosFlickerListener dfl, String url){
        final ArrayList<Album> albumes = new ArrayList<Album>();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                (response) -> {
                        try {
                            JSONObject photosets = new JSONObject(response.getJSONObject("photosets").toString());
                            JSONArray photoset = photosets.getJSONArray("photoset");
                            for (int i = 0; i < photoset.length(); i++) {
                                final Album album = new Album();
                                JSONObject albumTraido = photoset.getJSONObject(i);
                                JSONObject titulo = albumTraido.getJSONObject("title");
                                album.setId(albumTraido.getString("id"));
                                album.setOwner(albumTraido.getString("owner"));
                                album.setUsername(albumTraido.getString("username"));
                                album.setSecret(albumTraido.getString("secret"));
                                album.setTitle(titulo.getString("_content"));
                                albumes.add(album);
                            }
                            if(dfl != null){
                                dfl.onAlbumesLoaded(albumes);
                            }
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
        DataManagerAplication.getSharedQueue().add(jsonObjectRequest);

    }

    public static void loadFotos(Album album, DatosFlickerListener dfl){
        final ArrayList<Foto> fotos = new ArrayList<Foto>();
        String url = "https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=6e8a597cb502b7b95dbd46a46e25db8d&photoset_id="+album.getId()+ "&user_id="+ album.getOwner() +"&format=json&nojsoncallback=1";
        JsonObjectRequest jsonObjectRequestFotos = new JsonObjectRequest(Request.Method.GET, url, null,
                (response) -> {
                    try {
                        JSONObject photoset = new JSONObject(response.getJSONObject("photoset").toString());
                        JSONArray photo = photoset.getJSONArray("photo");
                        for (int i = 0; i < photo.length(); i++) {
                            final Foto foto = new Foto();
                            JSONObject fotoTraida = photo.getJSONObject(i);
                            foto.setIdFoto(fotoTraida.getString("id"));
                            foto.setSecret(fotoTraida.getString("secret"));
                            foto.setTitle(fotoTraida.getString("title"));
                            foto.setIdAlbum(album.getId());
                            fotos.add(foto);
                        }
                        if(dfl != null){
                            album.setIdPrimeraFoto(fotos.get(0).getIdFoto());
                            dfl.onFotosLoaded(fotos);
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("onErrorResponse", error.getMessage());
            }
        });

        DataManagerAplication.getSharedQueue().add(jsonObjectRequestFotos);
    }

    public static void loadComentarios(Foto foto, DatosFlickerListener dfl){
        final ArrayList<Comentario> comentarios = new ArrayList<Comentario>();
        String url = "https://api.flickr.com/services/rest/?method=flickr.photos.comments.getList&api_key=6e8a597cb502b7b95dbd46a46e25db8d&photo_id="+foto.getIdFoto()+"&format=json&nojsoncallback=1";
        JsonObjectRequest jsonObjectRequestComentarios = new JsonObjectRequest(Request.Method.GET, url, null,
                (response) -> {
                    try {
                        JSONObject commentObject = new JSONObject(response.getJSONObject("comments").toString());
                        JSONArray commentArray = commentObject.getJSONArray("comment");
                        for (int i = 0; i < commentArray.length(); i++) {
                            final Comentario comentario = new Comentario();
                            JSONObject comentarioTraido = commentArray.getJSONObject(i);
                            comentario.setIdComentario(comentarioTraido.getString("id"));
                            comentario.setContenidoComentario(comentarioTraido.getString("_content"));
                            comentario.setNombreAutor(comentarioTraido.getString("authorname"));
                            comentarios.add(comentario);
                        }
                        if(dfl != null){
                            dfl.onComentariosLoaded(comentarios);
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("onErrorResponseComentarios", error.getMessage());
            }
        });
        DataManagerAplication.getSharedQueue().add(jsonObjectRequestComentarios);
    }

}
