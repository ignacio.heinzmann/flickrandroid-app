package com.example.flickrapp.Datos;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.flickrapp.ClasesModelo.Album;
import com.example.flickrapp.ClasesModelo.Foto;

@Database(entities = {Foto.class, Album.class}, version = 6, exportSchema = false)
abstract class FlickrRoomDatabase extends RoomDatabase {

    abstract IAlbumDao albumDao();
    abstract IFotoDao fotoDao();

    public FlickrRoomDatabase(){

    }

}

