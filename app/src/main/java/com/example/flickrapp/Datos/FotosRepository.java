package com.example.flickrapp.Datos;

import android.content.Context;

import androidx.lifecycle.LiveData;

import com.example.flickrapp.ClasesModelo.Foto;

import java.util.List;

public class FotosRepository {
    private IFotoDao mIFotoDao;


    public FotosRepository(Context context) {
        FlickrRoomDatabase db = DatabaseProvider.getDatabase(context);
        mIFotoDao = db.fotoDao();
    }

    public LiveData<List<Foto>> getFotosByNombre(String idAlbum){
        return mIFotoDao.getFotosByNombre(idAlbum);
    }

    public LiveData<List<Foto>> getFotosByAlbumId(String idAlbum){
        return mIFotoDao.getFotosByAlbumId(idAlbum);
    }

    public void insert(final Foto foto) {
        new Thread() {
            @Override
            public void run() {
                mIFotoDao.insert(foto);
            }
        }.start();
    }

    public void update(final Foto foto) {
        new Thread() {
            @Override
            public void run() {
                mIFotoDao.update(foto);
            }
        }.start();
    }

    public void delete(final Foto foto) {
        new Thread() {
            @Override
            public void run() {
                mIFotoDao.delete(foto);
            }
        }.start();
    }
}
