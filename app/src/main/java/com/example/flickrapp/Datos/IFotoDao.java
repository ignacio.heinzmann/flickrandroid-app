package com.example.flickrapp.Datos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.flickrapp.ClasesModelo.Album;
import com.example.flickrapp.ClasesModelo.Foto;

import java.util.List;

@Dao
public interface IFotoDao {
    @Query("SELECT * from Fotos ORDER BY title ASC")
    LiveData<List<Foto>> getFotosByTitle();

    @Query("SELECT * from Fotos")
    LiveData<List<Foto>> getFotos();

    @Query("SELECT * from Fotos WHERE idAlbum = :idAlbum")
    LiveData<List<Foto>> getFotosByAlbumId(String idAlbum);

    @Query("SELECT * from Fotos WHERE idAlbum = :idAlbum ORDER BY title DESC")
    LiveData<List<Foto>> getFotosByNombre(String idAlbum);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Foto foto);

    @Update
    void update(Foto foto);

    @Delete
    void delete(Foto foto);

    @Query("DELETE FROM Fotos")
    void deleteAll();
}
