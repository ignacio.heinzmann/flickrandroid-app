package com.example.flickrapp.Datos;

import android.app.Application;
import android.content.Context;

import android.net.ConnectivityManager;


import com.android.volley.RequestQueue;

import com.android.volley.toolbox.Volley;

public class DataManagerAplication extends Application {

    private static RequestQueue queue;

    private FotosRepository fotosRepository;
    private AlbumesRepository albumesRepository;

    @Override
    public void onCreate() {
        super.onCreate();
        queue = Volley.newRequestQueue(this);
        fotosRepository = new FotosRepository(this);
        albumesRepository = new AlbumesRepository(this);
    }

    public static RequestQueue getSharedQueue() {
        return queue;
    }

    public FotosRepository getFotosRepository(){
        return fotosRepository;
    }

    public AlbumesRepository getAlbumesRepository(){
        return albumesRepository;
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

}
